'use strict';

class Game {

    /** Le constructeur
     * 
     */
    constructor(domZoneDisplay = null) {

        this.domZoneDisplay = (domZoneDisplay == null) ? document.querySelector('body') : domZoneDisplay;

        // intialisaton
        this.init();

        // On créé le plateau de jeu sur la page
        this.display();

    }

    /** Initialisation du jeu  */
    init() {
        /**
         * @var {Number} level niveau de difficulté
         */
        this.level = 1;

        /**
         * @var {Number} score score
         */
        this.score = 0;

        /**
         * @var {Number} speed vitesse de déplacement du serpent
         */
        this.speed = 1;

        /**
            * @var {Element} domObject objet du DOM représentant le plateau de jeu
         */
        this.domObject = null;

        /**
            * @var {Object} snake l'objet serppent
         */
        this.snake = new Snake();


        /** Start listener event Keyboard */
        document.addEventListener('keydown',this.play.bind(this));
    }

    /** Méthode qui affiche le plateau de jeu sur la page 
     * 
    */
    display() {
        console.log(this);
        this.domObject = document.createElementNS("http://www.w3.org/2000/svg", "svg");
        this.domObject.setAttribute('width', GAME_SIZE.x);
        this.domObject.setAttribute('height', GAME_SIZE.y);
        this.domObject.setAttribute('viewBox', `0 0 ${GAME_SIZE.boxSizeX} ${GAME_SIZE.boxSizeY}`);

        // On dessine le serpent dans la zone
        this.domObject.appendChild(this.snake.draw());

        // on ajoute le svg à la page
        this.domZoneDisplay.appendChild(this.domObject);

    }

    play(e) {
        console.log(this.snake.domObject);
        //console.log(e);
        this.snake.x +=1;
        this.snake.domObject.setAttribute('x', this.snake.x);
    }



}
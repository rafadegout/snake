'use strict';



class Snake {


    /** Le constructeur */
    constructor() {

        /**
         * @var {Number} x la position X du serpent
         */
        this.x = 0;

        /**
         * @var {Number} y la position Y du serpent
         */
        this.y = 0;

        /**
        * @var {Element} domObject l'objet du DOm représentant le serpent (la tête)
        */
        this.domObject = null;

        /**
        * @var {String} color la couleur du serpent
        */
        this.color = '#000000';
    }


    /** On représente le serpent sous la forme d'un objet du DOM
     * 
     */
    draw() {
        this.domObject = document.createElementNS("http://www.w3.org/2000/svg", "rect");
        this.domObject.setAttribute('x', this.x);
        this.domObject.setAttribute('y', this.y);
        this.domObject.setAttribute('width', 1);
        this.domObject.setAttribute('height', 1);
        this.domObject.setAttribute('style', `fill:${this.color}`);

        return this.domObject;
    }
}
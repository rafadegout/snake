'use strict';


const GAME_SIZE = {
    x : 600,
    y : 600,
    boxSizeX : 30,
    boxSizeY : 30
}


/**
 * @var {object} GAME_KEY touches du clavier qui sont interceptées
 */
const GAME_KEY = {
    right: 39,
    left: 37,
    up: 38,
    down: 40,
    pause: 32
};

/**
 * @var {object} DEFAULT_POSITION_SNAKE la position initiale du serpent
 */
const DEFAULT_POSITION_SNAKE = {
    x: 10,
    y: 10
};


document.addEventListener('DOMContentLoaded', ()=>{
    
    const game = new Game(document.querySelector('#game'));
    window.addEventListener("gamepadconnected", function(e) {
        console.log("Gamepad connected at index %d: %s. %d buttons, %d axes.",
          e.gamepad.index, e.gamepad.id,
          e.gamepad.buttons.length, e.gamepad.axes.length);
      });

});